﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Microsoft.WindowsAzure.MobileServices;
using GalaSoft.MvvmLight.Ioc;
using checkjump.Data;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


namespace checkjump.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();

			// Code for starting up the Xamarin Test Cloud Agent
			#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start();
			#endif
			CurrentPlatform.Init ();
			LoadApplication (new App ());

			UINavigationBar.Appearance.BarTintColor = UIColor.FromRGB(58,181,82); //bar background
			UINavigationBar.Appearance.TintColor = UIColor.White; //Tint color of button items
			UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
				{
					Font = UIFont.FromName("HelveticaNeue-Light", (nfloat)20f),
					TextColor = UIColor.White
				});

			UITabBar.Appearance.TintColor = UIColor.FromRGB (58, 181, 82);
			UITabBar.Appearance.BarTintColor = UIColor.White;

			return base.FinishedLaunching (app, options);
		}
			
	}
}

