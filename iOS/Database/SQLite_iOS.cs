﻿using System;
using checkjump;
using Xamarin.Forms;
using checkjump.iOS;
using System.IO;
using checkjump.Data;
using SQLite.Net;

[assembly: Dependency (typeof (SQLite_iOS))]

namespace checkjump.iOS
{
	public class SQLite_iOS : ISQLite
	{
		public SQLite_iOS ()
		{
		}

		#region ISQLite implementation
		public SQLiteConnection GetConnection ()
		{
			var sqliteFilename = "database.db3";
			string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
			string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
			var path = Path.Combine(libraryPath, sqliteFilename);
			var plat = new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS ();

			// This is where we copy in the prepopulated database
			Console.WriteLine (path);
			if (!File.Exists (path)) {
				File.Copy (sqliteFilename, path);
			}

			var conn = new SQLiteConnection(plat, path);

			// Return the database connection 
			return conn;
		}
		#endregion
	}
}