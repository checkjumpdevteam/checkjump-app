﻿using System;
using SQLite.Net;

namespace checkjump.Data
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();
	}
}

