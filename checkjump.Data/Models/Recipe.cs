﻿using System;

namespace checkjump.Data.Models
{
	public class Recipe
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Image { get; set; }
		public string Ingredients { get; set; }
		public string Howto { get; set; }

		public Recipe (int Id, string Name, string Image, string Ingredients, string Howto)
		{

			this.Id = Id;
			this.Name = Name;
			this.Image = Image;
			this.Ingredients = Ingredients;
			this.Howto = Howto;

		}
	}
}

