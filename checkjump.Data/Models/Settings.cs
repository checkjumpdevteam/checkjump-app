﻿using System;

namespace checkjump.Data
{
	public class Settings
	{

		public string SettingsName { get; set; }
		public string SettingsDesc { get; set; }

		public Settings (string settingsName, string settingsDesc)
		{
			SettingsName = settingsName;
			SettingsDesc = settingsDesc;
		}
	}
}

