﻿using System;

namespace checkjump.Data.Models
{
	public class Special
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Image { get; set; }

		public double RRP { get; set; }
		public string SalePrice { get; set; }

		public Special (int Id, string Name, string Image, double RRP, double SalePrice)
		{
			this.Id = Id;
			this.Name = Name;
			this.Image = Image;
			this.RRP = RRP;
			this.SalePrice = "$" + SalePrice + " was $" + RRP + " save $" + (RRP - SalePrice);
		}
	}
}

