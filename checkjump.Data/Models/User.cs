﻿using System;
using SQLite.Net.Attributes;
using SQLite;

namespace checkjump.Data
{
	public class User
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string BirthDate { get; set; }
		public int PostCode { get; set; }
		public int RewardsCardNumber { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public string ConfirmPassword { get; set; }

		public User () {
		}

		public User (
			int id, string firstname, string lastname, string birthdate, 
			int postcode, int rewardscardnumber, string email, 
			string password, string confirmpassword)
		{
			Id = id;
			FirstName = firstname;
			LastName = lastname;
			BirthDate = birthdate;
			PostCode = postcode;
			RewardsCardNumber = rewardscardnumber;
			Email = email;
			Password = password;
			ConfirmPassword = confirmpassword;
		}
	}
}

