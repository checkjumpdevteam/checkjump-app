﻿using System;
using SQLite.Net.Attributes;

namespace checkjump.Data.Models
{
	public class Product
	{
		[NotNull, PrimaryKey, AutoIncrement]
		public int Id { get; set; }
		[NotNull]
		public string ProductName { get; set; }
		[NotNull]
		public string ProductCategory { get; set; }
		public string ImageLocation { get; set; }
		public bool Allergy { get; set; }

		public int Aisle { get; set; }
		public double RRP { get; set; }
		public string SalePrice { get; set; }
		public int Quantity { get; set; }
		public double Weight { get; set; }
		public string Description { get; set; }


		public Product (int id, string productName, string productCategory, string imageLocation, bool allergy, int aisle, double rrp, double salePrice, int quantity, double weight)
		{
			Id = id;
			ProductName = productName;
			ProductCategory = productCategory;
			ImageLocation = imageLocation;
			Allergy = allergy;
			Aisle = aisle;
			RRP = rrp;
			SalePrice = "$" + salePrice;
			Quantity = quantity;
			Weight = weight;
			Description = quantity + " items, at $" + salePrice + " each.";
		}
	}
}

