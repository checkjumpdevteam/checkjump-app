﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using checkjump.Data;
using checkjump.Data.Models;
using System.Collections.ObjectModel;
using checkjump.Data.Models;

namespace checkjump.Data.ViewModel
{
	public class HomePageViewModel
	{
		private IMyNavigationService navigationService;

		public ObservableCollection<Product> MyList { get; set; }
		public ObservableCollection<Special> Specials { get; set; }
		public ObservableCollection<Recipe> Recipes { get; set; }

		public HomePageViewModel (IMyNavigationService navigationService)
		{
			this.navigationService = navigationService;
			MyList = new ObservableCollection<Product>
			{
				new Product(1, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5),

				new Product(6, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5),

				new Product(11, "Smith Sour Cream", "G", "smiths.jpg", false, 3, 5.99, 5.20, 0, 0.5),

				new Product(16, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5)
			};

			Specials = new ObservableCollection<Special> 
			{
				new Special(1, "Oreo Wafer", "Oreo.jpg", 2.50, 1.50),
				new Special(4, "Smith Sour Cream", "smiths.jpg", 3.00, 1.75),
				new Special(7, "Frozen Pizza", "pizzza.jpg", 4.50, 4.00)
			};

			
			Recipes = new ObservableCollection<Recipe> 
			{
				new Recipe(1, "Orange Chocolate Cake", "orangecake.jpg", "ingredients","how to make it"),
				new Recipe(2, "Orange Punch", "orangepunch.jpg", "ingredients","how to make it"),
				new Recipe(3, "Honey Chicken", "honeychicken.jpg", "ingredients","how to make it"),
				new Recipe(4, "Homemade Pizza", "pizza4.jpg", "ingredients","how to make it"),
				new Recipe(5, "California Rolls", "californiaroll.jpg", "ingredients","how to make it")

			};

		}
	}
}

