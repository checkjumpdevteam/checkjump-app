﻿using System;
using System.Collections.ObjectModel;
using checkjump.Data.Models;
using System.Windows.Input;
using Xamarin.Forms;
using checkjump.Data.ViewModel;
using System.Linq;
using checkjump.Data;

namespace checkjump.Data.ViewModel
{
	public class DeliProductViewModel
	{
		private IMyNavigationService navigationService;

		public ObservableCollection<Product> Products { get; set; }
		public ObservableCollection<Product> ChosenCategoryProducts { get; set; }
		private string category = "D";
		public string ProductName { get; set; }
		public DeliProductViewModel(IMyNavigationService navigationService)
		{
			this.navigationService = navigationService;

			Products = new ObservableCollection<Product>
			{
				new Product(1, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(1, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(1, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(1, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5),

				new Product(6, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(6, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(6, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(6, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5),

				new Product(11, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(11, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(11, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(11, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5),

				new Product(16, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(16, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(16, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(16, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5)
			};
			ChosenCategoryProducts = new ObservableCollection<Product> (Products.Where (t => t.ProductCategory == category));
		}
	}
}

