﻿using System;
using System.Windows.Input;
using checkjump.Data.Models;
using Xamarin.Forms;

namespace checkjump.Data.ViewModel
{
	public class ItemCell:ViewCell
	{
		public ItemCell()
		{
			var itemImage = new Image()
			{
				HeightRequest = 50,
				WidthRequest = 50,
				Aspect = Aspect.AspectFill
			};
			itemImage.SetBinding(Image.SourceProperty, "Image");

			var nameLabel = new Label()
			{
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 18,
				TextColor = Color.Black

			};
			nameLabel.SetBinding(Label.TextProperty, "Name");

			var salePrice = new Label()
			{
				FontAttributes = FontAttributes.Bold,
				FontSize = 12,
				TextColor = Color.FromHex("#666")
			};
			salePrice.SetBinding(
				Label.TextProperty, new Binding(
					"SalePrice",
					stringFormat: "${0}")
			);

			var rrp = new Label()
			{
				FontAttributes = FontAttributes.Bold,
				FontSize = 12,
				TextColor = Color.FromHex("#666")
			};
			rrp.SetBinding(
				Label.TextProperty, new Binding(
					"RRP",
					stringFormat: "Was ${0}")
			);

			var quantityLabel = new Label()
			{
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 18,
				TextColor = Color.Black,
				HorizontalOptions = LayoutOptions.EndAndExpand
			};
			quantityLabel.SetBinding(Label.TextProperty, "Quantity");


			// Set ViewCell layout
			var priceStack = new StackLayout()
			{
				Spacing = 2,
				Orientation = StackOrientation.Horizontal,
				Children = { salePrice, rrp }
			};

			var itemDetailsLayout = new StackLayout
			{
				Padding = new Thickness(10, 0, 0, 0),
				Spacing = 0,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { nameLabel, priceStack }
			};

			var cellLayout = new StackLayout
			{
				Spacing = 0,
				Padding = new Thickness(10, 5, 10, 5),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { itemImage, itemDetailsLayout, quantityLabel }
			};

			this.View = cellLayout;
		}
	}
}
