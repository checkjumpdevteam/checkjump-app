﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using checkjump.Data;
using checkjump.Data.Models;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;

namespace checkjump.Data.ViewModel
{
	public class CheckJumpViewModel
	{
		private IMyNavigationService navigationService;
		public ICommand AddProductWithCameraCommand { get; private set; }
		public ICommand RemProductWithCameraCommand { get; private set; }

		public ObservableCollection<Product> CheckJumpItems { get; set; }

		public CheckJumpViewModel (IMyNavigationService navigationService)
		{
			this.navigationService = navigationService;

			CheckJumpItems = new ObservableCollection<Product>
			{
				new Product(1, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(1, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(1, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(1, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5),

				new Product(6, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(6, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(6, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(6, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5),

				new Product(11, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(11, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(11, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(11, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5),

				new Product(16, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(16, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(16, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5),
				new Product(16, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5)
			};

			AddProductWithCameraCommand = new Command (async () => {
				var scanner = new ZXing.Mobile.MobileBarcodeScanner();
				var result = await scanner.Scan();
				if(result != null){
					MessagingCenter.Send(this, "MyAlertBarcode", result.Text);
				}
			});

			RemProductWithCameraCommand = new Command(async () => {
				var scanner = new ZXing.Mobile.MobileBarcodeScanner();
				var result = await scanner.Scan();
				if(result != null){
					MessagingCenter.Send(this,"MyAlertBarcodeRem", result.Text);
				}
			});

//			var CheckJumpItems = new List<Product> ();
//
//			CheckJumpItems.Add(new Product(1, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(2, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(3, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(4, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(5, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5));
//
//			CheckJumpItems.Add(new Product(6, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(7, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(8, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(9, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(10, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5));
//
//			CheckJumpItems.Add(new Product(11, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(12, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(13, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(14, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(15, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5));
//
//			CheckJumpItems.Add(new Product(16, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(17, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(18, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(19, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5));
//			CheckJumpItems.Add(new Product(20, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5));
//
//			List<Product> SortedCheckJumpItems = CheckJumpItems.OrderBy (o => o.Aisle).ToList ();

		}
	}
}

