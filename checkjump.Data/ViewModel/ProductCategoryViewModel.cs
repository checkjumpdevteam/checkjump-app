﻿using System;
using System.Windows.Input;
using checkjump.Data.Models;
using Xamarin.Forms;

namespace checkjump.Data.ViewModel
{
	public class ProductCategoryViewModel
	{
		private IMyNavigationService navigationService;

		public ICommand ProductListViewCommand { get; private set; }
		public ICommand SelectedProductCategoryCommand { get; private set; }



		public ProductCategoryViewModel (IMyNavigationService navigationService)
		{
			this.navigationService = navigationService;
			SelectedProductCategoryCommand = new Command(x => {


				switch (x.ToString()) {
				case "F":
					this.navigationService.NavigateTo (ViewModelLocator.freshproductlistPageKey);
					break;
			 	case "G":
					this.navigationService.NavigateTo (ViewModelLocator.groceryproductlistPageKey);
					break;
				case "M":
					this.navigationService.NavigateTo (ViewModelLocator.productlistPageKey);
					break;
				case "D":
					this.navigationService.NavigateTo (ViewModelLocator.deliproductlistPageKey);
					break;
				}
			});
				
		}
	}
}

