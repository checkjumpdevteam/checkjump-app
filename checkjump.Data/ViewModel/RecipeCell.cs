﻿using System;
using System.Windows.Input;
using checkjump.Data.Models;
using Xamarin.Forms;

namespace checkjump.Data.ViewModel
{
	public class RecipeCell:ViewCell
	{
		public RecipeCell ()
		{
			var recipeImage = new Image()
			{
				HeightRequest = 50,
				WidthRequest = 50,
				Aspect = Aspect.AspectFill
			};
			recipeImage.SetBinding(Image.SourceProperty, "Image");

			var nameLabel = new Label()
			{
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 18,
				TextColor = Color.Black
			};
			nameLabel.SetBinding(Label.TextProperty, "Name");

			// Set ViewCell layout
			var recipeDetailsLayout = new StackLayout
			{
				Padding = new Thickness(10, 7, 0, 0),
				Spacing = 0,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { nameLabel }
			};

			var cellLayout = new StackLayout
			{
				Spacing = 0,
				Padding = new Thickness(10, 5, 10, 5),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { recipeImage, recipeDetailsLayout,}
			};

			this.View = cellLayout;
		}
	}
}

