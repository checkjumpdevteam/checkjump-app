﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using checkjump.Data;
using checkjump.Data.Models;
using System.Collections.ObjectModel;

namespace checkjump.Data.ViewModel
{
	public class VirtualCartViewModel
	{
		private IMyNavigationService navigationService;
		public ObservableCollection<Product> VCart { get; set; }

		public VirtualCartViewModel (IMyNavigationService navigationService)
		{
			this.navigationService = navigationService;

			VCart = new ObservableCollection<Product> {
				new Product(1, "Sausages", "M", "sausages.jpg", false, 3, 5.99, 5.20, 0, 0.5),

				new Product(6, "Olives", "D", "olives.JPG", false, 3, 5.99, 5.20, 0, 0.5),

				new Product(11, "Smith Sour Cream", "G", "chips.jpg", false, 3, 5.99, 5.20, 0, 0.5),

				new Product(16, "Apples", "F", "apple.jpeg", false, 3, 5.99, 5.20, 0, 0.5)
			};
		}

	}
}

