﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using checkjump.Data;
using checkjump.Data.Models;

namespace checkjump.Data.ViewModel
{
	public class LogInViewModel
	{
		private IMyNavigationService navigationService;
		public ICommand LogInCommand { get; private set; }
		public ICommand NewSignUpCommand { get; private set; }

		public LogInViewModel (IMyNavigationService navigationService)
		{
			this.navigationService = navigationService;
			// if log in button is clicked, navigate to main tab view page
			LogInCommand = new Command (() => this.navigationService.NavigateTo (ViewModelLocator.navigationPageKey));
			NewSignUpCommand = new Command (() => this.navigationService.NavigateTo (ViewModelLocator.signupPageKey));
		}
	}
}


