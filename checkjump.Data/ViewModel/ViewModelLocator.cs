/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:checkjump.Data"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using checkjump.Data;
using checkjump.Data.ViewModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace checkjump.Data.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
		// log in page
		public const string loginPageKey = "login";
		// sign up page
		public const string signupPageKey = "signup";
		// "home" page
		public const string homePageKey = "home";
		//navigation page
		public const string navigationPageKey = "navigation";
		// product category page
		public const string productcategoryPageKey = "products";
		// page that lists products after category has been selected
		public const string productlistPageKey = "productlist";
		public const string freshproductlistPageKey = "freshproductlist";
		public const string deliproductlistPageKey = "deliproductlist";
		public const string groceryproductlistPageKey = "groceryproductlist";
		// virtual cart page
		public const string virtualcartPageKey = "virtualCart";
		// checkjump page
		public const string checkjumpPageKey = "checkjump";
		// account settings page
		public const string accountPageKey = "account";

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

			// log in page
			SimpleIoc.Default.Register<LogInViewModel>(() => 
				{
					return new LogInViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});
					
			// sign up page
			SimpleIoc.Default.Register<SignUpViewModel>(() => 
				{
					return new SignUpViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});
			
			// "home" page
			SimpleIoc.Default.Register<HomePageViewModel>(() => 
				{
					return new HomePageViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});
			
			//navigation page
			SimpleIoc.Default.Register<NavigationViewModel>(() => 
				{
					return new NavigationViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});
			
			// product category page
			SimpleIoc.Default.Register<ProductCategoryViewModel>(() => 
				{
					return new ProductCategoryViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});
			
			// product list page
			SimpleIoc.Default.Register<ProductViewModel>(() => 
				{
					return new ProductViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});

			SimpleIoc.Default.Register<FreshProductViewModel>(() => 
				{
					return new FreshProductViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});

			SimpleIoc.Default.Register<DeliProductViewModel>(() => 
				{
					return new DeliProductViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});

			SimpleIoc.Default.Register<GroceryProductViewModel>(() => 
				{
					return new GroceryProductViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});
			
			// virtual cart page
			SimpleIoc.Default.Register<VirtualCartViewModel>(() => 
				{
					return new VirtualCartViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});
			
			// checkjump page
			SimpleIoc.Default.Register<CheckJumpViewModel>(() => 
				{
					return new CheckJumpViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});

			// account settings page
			SimpleIoc.Default.Register<AccountViewModel>(() => 
				{
					return new AccountViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});

		}


	// log in page
	public LogInViewModel login
	{
		get
		{
			return ServiceLocator.Current.GetInstance<LogInViewModel>();
		}
	}

	// sign up page
	public SignUpViewModel signup
	{
		get
		{
			return ServiceLocator.Current.GetInstance<SignUpViewModel>();
		}
	}

	// "home" page
	public HomePageViewModel home
	{
		get
		{
			return ServiceLocator.Current.GetInstance<HomePageViewModel>();
		}
	}

	//navigation page
	public NavigationViewModel navigation
	{
		get
		{
			return ServiceLocator.Current.GetInstance<NavigationViewModel>();
		}
	}

	// product category page
	public ProductCategoryViewModel products // change to ProductCategory
	{
		get
		{
			return ServiceLocator.Current.GetInstance<ProductCategoryViewModel>();
		}
	}

	// product list page
	public ProductViewModel productlist
	{
		get
		{
			return ServiceLocator.Current.GetInstance<ProductViewModel>();
		}
	}

	public FreshProductViewModel freshproductlist
	{
		get
		{
			return ServiceLocator.Current.GetInstance<FreshProductViewModel>();
		}
	}

		public DeliProductViewModel deliproductlist
		{
			get
			{
				return ServiceLocator.Current.GetInstance<DeliProductViewModel>();
			}
		}


		public GroceryProductViewModel groceryproductlist
		{
			get
			{
				return ServiceLocator.Current.GetInstance<GroceryProductViewModel>();
			}
		}


	// virtual cart page
	public VirtualCartViewModel virtualcart
	{
		get
		{
			return ServiceLocator.Current.GetInstance<VirtualCartViewModel>();
		}
	}

	// checkjump page
	public CheckJumpViewModel checkjump
	{
		get
		{
			return ServiceLocator.Current.GetInstance<CheckJumpViewModel>();
		}
	}

	// checkjump page
		public AccountViewModel account
	{
		get
		{
			return ServiceLocator.Current.GetInstance<AccountViewModel>();
		}
	}

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}