﻿using System;
using System.Collections.ObjectModel;

namespace checkjump.Data
{
	public class AccountViewModel
	{
		private IMyNavigationService navigationService;
		public ObservableCollection<Settings> Settings { get; set; }
		public string SettingsName { get; set; }

		public AccountViewModel (IMyNavigationService navigationService)
		{
			this.navigationService = navigationService;
			SettingsName = "Settings from MVVM";
			Settings = new ObservableCollection<Settings> {
				new Settings ("  Account", ""), 
				new Settings ("", "Everyday Rewards Card"),
				new Settings ("", "Account Info"),
				new Settings ("", "Shop Preferences"),
				new Settings ("  Payment", ""),
				new Settings ("", "PayPayl"),
				new Settings ("", "Credit Card"), 
				new Settings ("  CheckJump", ""),
				new Settings ("", "Notifications"),
				new Settings ("", "Permissions"),
				new Settings ("", "About"),
			};
		}
	}
}

