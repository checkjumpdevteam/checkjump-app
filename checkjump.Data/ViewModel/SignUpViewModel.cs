﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using checkjump.Data;
using checkjump.Data.Models;
using checkjump.Data.ViewModel;
using Microsoft.Practices.ServiceLocation;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using GalaSoft.MvvmLight;
using checkjump;

namespace checkjump.Data
{
	public class SignUpViewModel : ViewModelBase  
	{
		private IMyNavigationService navigationService;
		public ICommand RegisterUserCommand { get; private set; }
		private ObservableCollection<User> userList { get; set; }
		public ObservableCollection<User> UserList {
			get { return userList; }
			set {
				userList = value;
			}
		}

		public ICommand UserListCommand { get; private set; }

		public String FirstName 
		{
			get {return firstName;}
			set {
				firstName = value;
//				RaisePropertyChanged (() => FirstName);
			}
		}

		private string firstName;

		public String LastName 
		{
			get {return lastName;}
			set {
				lastName = value;
//				RaisePropertyChanged (() => lastName);
			}
		}

		private string lastName;

		public int PostCode 
		{
			get {return postCode;}
			set {
				postCode = value;
//				RaisePropertyChanged (() => postCode);
			}
		}

		private int postCode;

		public int RewardsCardNumber 
		{
			get {return rewardscardnumber;}
			set {
				rewardscardnumber = value;
//				RaisePropertyChanged (() => postCode);
			}
		}

		private int rewardscardnumber;

		public string Email 
		{ 
			get { return email; }
			set {
				email = value;
//				RaisePropertyChanged (() => email);
			}
		}

		private string email;

		public string Password
		{
			get { return password; }
			set {
				password = value;
//				RaisePropertyChanged (() => password);
			}
		}
			
		private string password;

		public string ConfirmPassword
		{
			get { return confirmPassword; }
			set {
				confirmPassword = value;
//				RaisePropertyChanged (() => confirmPassword);
			}
		}

		private string confirmPassword;

		public SignUpViewModel (IMyNavigationService navigationService)
		{
			this.navigationService = navigationService;
//			var database = new CheckJumpDatabase();
			RegisterUserCommand = new Command (() => {
				navigationService.NavigateTo (ViewModelLocator.navigationPageKey);
//				App.Database.GetAll();
//				database.InsertOrUpdateUser(new User(1, FirstName, LastName, "birthdate", 
//					PostCode, RewardsCardNumber, Email, 
//					Password, ConfirmPassword));
//				List<User> users = database.GetAll();
			});

		}
	}
}

