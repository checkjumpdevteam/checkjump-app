﻿using System;
using SQLite.Net;
using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Ioc;
using checkjump.Data.Models;
using Microsoft.WindowsAzure.MobileServices;
using System.Threading.Tasks;

namespace checkjump.Data
{
	public class CheckJumpDatabase
	{

		public static MobileServiceClient MobileService = new MobileServiceClient (
			"https://checkjump-app.azure-mobile.net/",
			"njJubZDTYlQCobobsfbFMsjphAMkhJ70"
		);

		SQLiteConnection database;

		public CheckJumpDatabase ()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();

			//			if (database.TableMappings.All(t => t.MappedType.Name != typeof(Product).Name)) {
			//				database.CreateTable<Product> ();
			//				database.Commit ();
			//			}
			if (database.TableMappings.All(t => t.MappedType.Name != typeof(User).Name)) {
				database.CreateTable<User> ();
				database.Commit ();
			}
		}

		//		// get all products from database
		//		public async Task<List<Product>> GetAllProducts(){
		//			var x = await MobileService.GetTable<Product> ().ToListAsync ();
		//			return x;
		//		}
		//
		//		// Insert a product 
		//		public async Task<int> InsertOrUpdateProduct(Product product){
		//			var lookup = await MobileService.GetTable<Product> ().LookupAsync (product.Id);
		//			if (lookup != null) {
		//				await MobileService.GetTable<Product> ().InsertAsync (product);
		//			} else {
		//				await MobileService.GetTable<Product> ().UpdateAsync (product);
		//			}
		//			return 1;
		//		}

		public async Task<List<User>> GetAllUsers(){
			var x = await MobileService.GetTable<User> ().ToListAsync ();
			return x;
		}

		public async Task<int> InsertOrUpdateUser(User user){
			var lookup = await MobileService.GetTable<User> ().LookupAsync (user.Id);
			if (lookup != null) {
				await MobileService.GetTable<User> ().InsertAsync (user);
			} else {
				await MobileService.GetTable<User> ().UpdateAsync (user);
			}
			return 1;
		}

		//		public User GetUser(string key){
		//			return database.Table<User> ().First (t => t.Email == key); 
		//		}

	}
}


