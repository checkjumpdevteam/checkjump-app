﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Microsoft.WindowsAzure.MobileServices;
using GalaSoft.MvvmLight.Ioc;
using checkjump.Data;

namespace checkjump.Droid
{
	[Activity (Label = "CheckJump", Theme = "@style/Theme", Icon="@drawable/check", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)] 
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);
			//TODO show IOC vs Dependency injection
			CurrentPlatform.Init();
			LoadApplication (new App ());

		}
	}
}

