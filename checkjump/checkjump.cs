﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using checkjump.Views;
using Xamarin.Forms;
using checkjump.Data.ViewModel;
using GalaSoft.MvvmLight.Ioc;
using checkjump.Data;

namespace checkjump
{
	public class App : Application
	{
		public static ViewModelLocator _locator;
		private static NavigationService nav;
		public static ViewModelLocator Locator
		{
			get 
			{
				return _locator ?? (_locator = new ViewModelLocator ());
			}
		}

		public App ()
		{
			MainPage = GetMainPage ();
		}


		public Page GetMainPage()
		{
			nav = new NavigationService ();
			// log in page
			nav.Configure (ViewModelLocator.loginPageKey, typeof(login));
			// sign up page
			nav.Configure (ViewModelLocator.signupPageKey, typeof(signup));
			// "home" page
			nav.Configure (ViewModelLocator.homePageKey, typeof(home));
			// navigation page
			nav.Configure (ViewModelLocator.navigationPageKey, typeof(navigation));
			// product category page
			nav.Configure (ViewModelLocator.productcategoryPageKey, typeof(products));
			// product list page
			nav.Configure (ViewModelLocator.productlistPageKey, typeof(productlist));
			nav.Configure (ViewModelLocator.freshproductlistPageKey, typeof(freshproductlist));
			nav.Configure (ViewModelLocator.deliproductlistPageKey, typeof(deliproductlist));
			nav.Configure (ViewModelLocator.groceryproductlistPageKey, typeof(groceryproductlist));
			// virtual cart page
			nav.Configure (ViewModelLocator.virtualcartPageKey, typeof(virtualCart));
			// checkjump page
			nav.Configure (ViewModelLocator.checkjumpPageKey, typeof(checkJump));
			// account page 
			nav.Configure (ViewModelLocator.accountPageKey, typeof(account));

			SimpleIoc.Default.Register<IMyNavigationService> (()=> nav, true);
			var navPage = new NavigationPage (new login());
			nav.Initialize (navPage);
			return navPage;
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

