﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace checkjump.Views
{
	public partial class navigation : TabbedPage
	{
		public navigation()
		{
			BindingContext = App.Locator.navigation;
			Title = "CheckJump";

			Children.Add(new home()
//				{Icon="House34.png"}
			);
			Children.Add(new products()
//				{Icon="checking1.png"}
			);
			Children.Add(new checkJump()
//				{Icon="happy50.png"}
			);
			Children.Add(new virtualCart()
//				{Icon="marker20.png"}
			);
			Children.Add (new account ()
//				{Icon="account4.png"}
			);
		}
	}
}
