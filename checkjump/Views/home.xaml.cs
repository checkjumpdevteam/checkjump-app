﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using checkjump.Data.Models;
using checkjump.Data.ViewModel;
using Xamarin.Forms;

namespace checkjump.Views
{
	public partial class home : ContentPage
	{
		public home()
		{
			InitializeComponent();
			BindingContext = App.Locator.home;

		}
	}
}
