﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using checkjump.Views;
using Xamarin.Forms;
using checkjump.Data.ViewModel;
namespace checkjump.Views
{
	public partial class virtualCart : ContentPage
	{
		public virtualCart ()
		{
			InitializeComponent ();
			BindingContext = App.Locator.virtualcart;
			button1.Clicked += async (sender, e) => {
				DisplayAlert ("Payment Complete", "Thank you for shopping with Woolworths", "OK");
			};
		}
	}
}	