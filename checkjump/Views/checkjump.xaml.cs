﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using checkjump.Data.ViewModel;
using checkjump.Data.Models;
using Xamarin.Forms;

namespace checkjump.Views
{
	public partial class checkJump : ContentPage
	{
		public checkJump()
		{
			InitializeComponent ();
			BindingContext = App.Locator.checkjump;
			//pass barcode text to display on screen
			MessagingCenter.Subscribe<CheckJumpViewModel, string>(this, "MyAlertBarcode", (sender,arg) =>{
				DisplayAlert("Item Added", arg, "Cancel");	
			});

			MessagingCenter.Subscribe<CheckJumpViewModel, string>(this, "MyAlertBarcodeRem", (sender,arg) =>{
				DisplayAlert("Item Removed", arg, "Cancel");	
			});

			nfcbutton.Clicked += async (sender, e) => {
				DisplayAlert ("NFC Setting", "BETA Version 0.98 still to implement NFC connection!", "OK");

			};
		}
	}
}
