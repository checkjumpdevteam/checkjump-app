﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using checkjump.Views;
using Xamarin.Forms;
using checkjump.Data.ViewModel;

namespace checkjump.Views
{
	public partial class account : ContentPage
	{
		public account ()
		{
			InitializeComponent ();
			BindingContext = App.Locator.account;
		}
	}
}

