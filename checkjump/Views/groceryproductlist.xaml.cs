﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace checkjump.Views
{
	public partial class groceryproductlist : ContentPage
	{
		public groceryproductlist ()
		{
			InitializeComponent ();
			BindingContext = App.Locator.groceryproductlist;
		}
	}
}

